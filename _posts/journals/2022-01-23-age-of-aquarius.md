---
title: 'Age of Aquarius'
date: '2022-01-22'
category: 'Journal'
tags: ['personal', '2022', 'self-improvement', 'meditation']
toc: false
---

Hey fellow Internet denizens, it's been a long while since my last post but what can I say? 2021 was just as bad as 2020, life is a whirlwind and you never know where it might take you! While I have been in Arizona for the last few years, but will be heading back home to the east coast in the near future. Home is always for the homesick and I have missed old friends quite terribly, so at least it will be nice to see them again while I am back taking care of a few things!

I've have made some hosting and structural changes to the blog behind the scenes and I have rexamined my ideas for the blog itself. I recently learned about the term 'digital garden' and I'm a bit surprised I haven't heard of it before given it's obviousness, especially in relation to actual horticulture and permaculture. People, systems, software, practices, ideas, opinions, beliefs all grow and change. This blog is built as a version controled JAMstack, which has some built in advantages like being easy to edit and update and is somewhat coupled to regular development workflows. It seems like a natural progression to make the blog itself a living garden rather than just a chapter book. For major updates or revisions to article content I will include a changelog on the page in question, but minutia such as spelling errors probably won't be noted as such edits can still be viewed in the repository's commit history. I'd like to have a few handy pages dedicated to useful bookmarks or 'maps of content' and I've been thinking of a dedicated changelog page as well to note most recent updates to the different articles.

[![Hex](/assets/content/misc/hex-dan-luvisi.jpg){: .right width='250' }](https://www.artstation.com/danluvisiart)

I've been making progress on some personal goals over the last few years but the path to self-betterment is one neverending. My main goal this year is to increase my overall productivity without turning into a manic wreck and bringing more balance into my life. Yoga and martial arts from college would be really nice right about now if I could find time 🙁. My coding endeavors this year include diving into a lot more data visualization, Kubernetes, Ansible, getting better at web development, and picking up some Rust. Writing more here is always on the list, of course. While the best laid plans often go astray and all our resolutions always tend to fray, it is of paramount importance that we have them all the same, and after all... self-improvement is never not a noble naivety 😉 Only from failure do we grow, measured in milestones and a weathered watch of timand transference. An ever observant observer of the obverse.

And thus we arrive at ourselves and what we ought to do about them. We have come so far, but we stand in troubled waters. We stand before a very exciting and terrifying inflection point at the precipice of history... Behind us slumber the lessons we have yet to learn, beyond us stands a door. A door into our deepest dreams, and all our devils darkly. A door of darkness unseen, or a door to light and hope. That choice is ours, it's always ours, but together we must make it. Together, we step into another year, into the turning of the wheel, into the Kali Yuga, into the 'Age of Aquarius'. Together, let us make use of this new year, this new breath. Let us put this time towards the betterment of ourselves and not the battering of each other. Let us take this year to make a real effort to grow a as a participant of this realm and rise above our plagues and the collective matrix we have made for ourselves. Let us walk hand in hand into a new aeon of understanding and wonder!

Reach out where you normally might not. Go volunteer... not one, not twice, but put in a real effort. Detrash, degunk, and clean up your community. Learn that new language, then actually go use it! To paraphrase John Muir, get out and have a good look at the world before it gets dark.

Take your chances, heal what wounds you, and come out of that shell... the world is calling.

See you out there,
David.

![The Journey Begins, Cameron Gray](/assets/content/misc/the_journey_begins.webp){: .center }
_[The Journey Begins, Cameron Gray](https://www.parablevisions.com/)_
