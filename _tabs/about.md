---
title: About Me
# icon: fas fa-info
icon: fas fa-info-circle
order: 2
---

# 🧑‍💻 About the author
Hi, I'm David. I'm a teacher, coder, and all around tech nerd. I've worked in many areas of the tech sector from IT, Systems Administration, Cybersecurity, Software Development (Java/Android, Web), to Education and EdTech and would consider myself a generalist, though we all have our specialties 😸! I'm mostly interested in information visualization, 3D and spatial interfaces, AI and automation, human-computer interaction, cybersecurity, digital archiving, open source and open information, and digital citizenship. Outside of tech, I'm quite fond of the outdoors and one day I'd like to have a little off-grid homestead so I'm always looking for neat low-tech or off-grid solutions. I enjoy camping, hiking, exploring nature, and unplugging from society whenever possible, learning about different cultures and histories, video gaming, punk rock, and probably most activities that come to mind when you think of the phrase 🎢"adrenaline junkie"!

<br>

# 📘 Bloglaimers
This is my dev blog. It is in part a journal and digital garden, a place to collect myself and my thoughts (in what will probably be a terribly uncollected manner), collections of guides, useful bookmarks and indexed maps of content around the web, public personal projects, and whatever else I think would be interesting to put here.

Topics are subject to vary at my whims but generally explore:
- 🤖 Technology and 📟 Computer Science
- 💻 Software Engineering and 🖥️ Systems Administration
- 🧮 Mathematics, ⚛️ Physics, 🧑‍🔬 Science, and 🪐 Space stuff!
- 🤔 Philosophy, ♉ Theology, and 🕉️ Religion
- 💬 Linguistics, 🔣 Etymology
- 🎮 Gaming and 🖖 nerd stuff
- ☯️ Ecology and 🌱 Permaculture
- 🎧 Music, 🎨 Art, and 📺 Media

This website and it's content should be viewed as living entities. Overtime, but rarely, I might change the face of the website, more uncommonly I might add easter eggs or new features (and bugs 🙁), and generally the content of posts will generally be updated to reflect changes and corrections over time (though, I will link an associated changelog for archival). This site is an experimental sandbox. Some posts will be technical walkthroughs, some will be personal journals, others may be an index to other useful things around the web, or postings and reflections of things I'm actively learning myself. I hope that in the chaos of it all you might find some useful bits and bytes and insights to help you on your own journey.

<div>
  <i class="fa fa-github" aria-hidden="true"></i>
</div>

<div align="center">
  <h1>Find me on the net</h1>
  <div>
    <a aria-label="GitHub" target="_blank" href="https://github.com/dcalano"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/github/github-original.svg" width="64px" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
    <a aria-label="LinkedIn" target="_blank" href="https://www.linkedin.com/in/davidcalano/"><img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/linkedin/linkedin-original.svg" width="64px" /></a> &nbsp;&nbsp;&nbsp;&nbsp;
  </div>
</div>

<br>

![Torus](/assets/img/torus.webp){: .center w="600" }
